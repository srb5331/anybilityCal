/*
 * Copyright (c) Microsoft All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

var http = require('request');
var Q = require('q');
var config = require('./config');

// The auth module object.
var auth = {};

// @name getAccessToken
// @desc Makes a request for a token using client credentials.
auth.getAccessToken = function (request, response, callback) {

  // These are the parameters necessary for the OAuth 2.0 Client Credentials Grant Flow.
  // For more information, see Service to Service Calls Using Client Credentials (https://msdn.microsoft.com/library/azure/dn645543.aspx).
  var requestParams = {
    grant_type: 'client_credentials',
    client_id: config.clientId,
    client_secret: config.clientSecret,
    resource: 'https://graph.microsoft.com'
  };

    // Make a request to the token issuing endpoint.
    token = http.post({ url: config.tokenEndpoint, form: requestParams }, function (err, response, body) {
        var parsedBody = JSON.parse(body);

        if (err) {
          //deferred.reject(err);
          callback(err, null);
        } else if (parsedBody.error) {
          //deferred.reject(parsedBody.error_description);
          callback(parsedBody.error, null);
        } else {
          // If successful, return the access token.
          token = parsedBody.access_token;
          callback(null, token);
          //deferred.resolve(parsedBody.access_token);
        }
  });
};

module.exports = auth;
